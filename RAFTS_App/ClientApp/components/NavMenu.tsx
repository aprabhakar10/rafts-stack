import * as React from 'react';
import { NavLink, Link } from 'react-router-dom';

export class NavMenu extends React.Component<{}, {}> {
    public render() {
        return (
            <div className='main-nav'>
                <div className='navbar navbar-inverse'>
                    <div className='navbar-container'>
                        <div className='navbar-header'>
                            <button type='button' className='navbar-toggle' data-toggle='collapse' data-target='.navbar-collapse'>
                                <span className='sr-only'>Toggle navigation</span>
                                <span className='icon-bar'></span>
                                <span className='icon-bar'></span>
                                <span className='icon-bar'></span>
                            </button>
                            <Link className='navbar-brand' to={'/'} data-toggle="collapse" data-target=".navbar-collapse.in">RAFTS</Link>
                        </div>
                        <div className='navbar-collapse collapse'>
                            <div className='navbar-collapse-container'>
                                <ul className='nav navbar-nav'>
                                    <li>
                                        <NavLink to={'/counter'} activeClassName='active' data-toggle="collapse" data-target=".navbar-collapse.in">Counter</NavLink>
                                    </li>
                                    <li>
                                        <NavLink to={'/weatherforecast'} activeClassName='active' data-toggle="collapse" data-target=".navbar-collapse.in">Weather Forecast</NavLink>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
