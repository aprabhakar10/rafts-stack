import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';

export default class Home extends React.Component<RouteComponentProps<{}>, {}> {
    public render() {
        return (
            <div>
                <div className="jumbotron-container">
                    <div className='home-jumbotron'>
                        <div>
                            <h1>React</h1>
                            <h1>ASP.NET Core</h1>
                            <h1>F #</h1>
                            <h1>Type Providers</h1>
                            <h1>SQL Server</h1>
                        </div>
                    </div>
                </div>
                <div className="container">
                    <h1>Welcome</h1>
                    <p>Welcome to your new single-page application, built with:</p>
                    <ul>
                        <li><a href='https://facebook.github.io/react/' target="_blank">React</a>, <a href='http://redux.js.org' target="_blank">Redux</a>, and <a href='http://www.typescriptlang.org/' target="_blank">TypeScript</a> for client-side code</li>
                        <li><a href='http://getbootstrap.com/' target="_blank">Bootstrap</a> for layout and styling</li>
                        <li><a href='https://webpack.github.io/' target="_blank">Webpack</a> for building and bundling client-side resources</li>
                        <li><a href='https://get.asp.net/' target="_blank">ASP.NET Core</a> and <a href='https://msdn.microsoft.com/en-us/library/67ef8sbd.aspx' target="_blank">C#</a> for cross-platform server-side code</li>
                        <li><a href='https://docs.microsoft.com/en-us/dotnet/fsharp/tutorials/type-providers/' target="_blank">F# Type Providers</a> for data-access</li>
                        <li><a href="https://www.microsoft.com/en-us/sql-server/" target="_blank">SQL Server</a> for relational data storage</li>
                    </ul>
                    <p>To help you, we've also set up:</p>
                    <ul>
                        <li><strong>Client-side navigation</strong>. For example, click <em>Counter</em> then <em>Back</em> to return here.</li>
                        <li><strong>Webpack dev middleware</strong>. In development mode, there's no need to run the <code>webpack</code> build tool. Your client-side resources are dynamically built on demand. Updates are available as soon as you modify any file.</li>
                        <li><strong>Hot module replacement</strong>. In development mode, you don't even need to reload the page after making most changes. Within seconds of saving changes to files, rebuilt React components will be injected directly into your running application, preserving its live state.</li>
                        <li><strong>Efficient production builds</strong>. In production mode, development-time features are disabled, and the <code>webpack</code> build tool produces minified static CSS and JavaScript files.</li>
                        <li><strong>Server-side prerendering</strong>. To optimize startup time, your React application is first rendered on the server. The initial HTML and state is then transferred to the browser, where client-side code picks up where the server left off.</li>
                    </ul>
                </div>
            </div>
        );
    }
}
