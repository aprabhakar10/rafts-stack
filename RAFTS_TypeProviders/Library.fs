namespace RAFTS_TypeProviders

module Say =
    let hello name =
        printfn "Hello %s" name
